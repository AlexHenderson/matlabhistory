# MatlabHistory

## Overview
These files add a [Git](http://git-scm.com/) repository to your MATLAB Command History and other user settings. 

## Rationale
MATLAB will record commands entered in the Command Window. 
These are stored in a file named `history.m` hidden away in your operating system. 
While MATLAB can store a large number of commands, once the space is full, the oldest commands are removed. 
This means that if you wish to step back in time to see how something was done a while ago, the relevant commands may not be available. 
This project adds a Git repository to the settings area and records the command history each time MATLAB is closed. 

You might never need this functionality, but if you do, you’ll be pleased you activated it! 

## Steps
There are three actions that need to be taken

1. Install Git
2. Activate the repository
3. Place code in a location that will be executed at MATLAB shutdown

These steps are outlined below. 

## Limitations and mitigation
+ Only available for Microsoft Windows (at the moment)
+ The Git repository is located in the 'Documents and Settings' area of your user profile. 
The code can easily be changed to use a remote repository, but this is not the current operation. 
+ If you wish to keep your command history when moving to another computer, you will have to copy the hidden `.git` 
folder to the new installation and restore the embedded history using Git commands. This code does not do that. 
+ If you wish to extract your commands from a long time ago (that MATLAB has deleted, but that Git has remembered) 
you will have to do that using Git commands. This code does not do that. 
+ Each Microsoft Windows user has their own command history; therefore each will need their own Git repository. 
This code could be modified to allow for different MATLAB users who operate using the same Windows account to 
have their own separate command history. This code does not do that. 
+ Only tested on Windows 7.

---
## Usage
### Step 1. Install Git
Download and install Git. The Git program can be downloaded from [http://git-scm.com/](http://git-scm.com/). You will need Administrator privileges. 
 
During installation, most of the default settings are OK. However, you should consider two changes.

+ On the screen named 'Adjusting your PATH environment' select 'Run Git from the Windows Command Prompt'. 
This means we do not need to specify the actual location of the Git program; it will be found automatically.
+ On the screen named 'Configuring the line ending conversions' select the 'Commit as Unix and retrieve as Windows' option (or something like that). 
This may make life easier if moving to a different operating system in the future. 

### Step 2. Activate the repository
Open MATLAB and run the `AddGitRepo.m` script. You should only need to do this once. Repeating the action will not damage anything. 

### Step 3. Place code in a location that will be executed at MATLAB shutdown
When MATLAB closes, a special file called `finish.m` is executed. 
More information on this can be found on the MathWorks website 
[http://www.mathworks.co.uk/help/matlab/ref/finish.html]( http://www.mathworks.co.uk/help/matlab/ref/finish.html). 

The MATLAB code that adds the command history to the Git repository is called `RecordHistory.m`

There are two scenarios:

1. A `finish.m` file already exists on your MATLAB path. 
Since *finish.m* is not present by default, you need to assess what the *finish.m* file is already doing and make sure that 
you are not going to break the current system. 
Discuss with colleagues why such a file is present and its purpose. 
If there are no issues, the following line can be added to the existing *finish.m* file; probably at the end.         
`[result] = RecordHistory();`

2. There is no `finish.m` file on your MATLAB path. 
In this case you should create a file called `finish.m` and save it on your MATLAB path. 
A suggested location is in the `local` folder which is in the `toolbox` folder of your MATLAB installation, 
however anywhere on the MATLAB path is acceptable. The following line should be added to this `finish.m` file.        
`[result] = RecordHistory();`

In either scenario the `RecordHistory.m` file needs to be on your MATLAB path. 
A suggested location is in the `local` folder which is in the `toolbox` folder of your MATLAB installation, 
however anywhere on the MATLAB path is acceptable.

Once these files have been edited/copied you may need to refresh the toolbox cache. 
Restarting MATLAB will do this, or you can run the following command

`rehash toolboxcache;`

That's it! When you next run MATLAB and close your session (exiting MATLAB), your command history will be stored in the Git repository. 

---
## Author(s)
Alex Henderson (<alex.henderson@manchester.ac.uk>)

## Licence
Creative Commons Attribution (CC BY), 
[Overview of licence](https://creativecommons.org/licenses/by/4.0/), 
[Full licence](https://creativecommons.org/licenses/by/4.0/legalcode)

## Disclaimer
These files and code therein are used at your own risk and the authors accept no responsibility for any data loss or damage caused. 

## Version history
**Version 1.0, April 2014**

Initial release. Alex Henderson

*\*\*Document ends\*\**
