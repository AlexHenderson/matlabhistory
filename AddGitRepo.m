function [resultInit,resultAdd] = AddGitRepo()

%AddGitRepo Adds Git version control to the MATLAB Command History

% Use in conjunction with finish.m and RecordHistory.m
% Alex Henderson (alex.henderson @ manchester.ac.uk) 2014
% Released under Creative Commons CC-BY licence
% See https://bitbucket.org/AlexHenderson/matlabhistory

% Requires an installation of Git available on the system path (not the
% MATLAB path)
% More information on Git version control here: http://git-scm.com/

% Version 1.0 2014-04-15
%   initial release

%% Check we're running Windows. These commands are OS specific. 
if (~ispc)
    error('These installation steps are for Windows only. Similar mechanisms may be available for Linux and Macintosh');
end

%% Gather pertinent information about the user and MATLAB version
homeLocation=getenv('USERPROFILE');
release=version('-release');

%% Move to location of the MATLAB Command History
historyLocation = [homeLocation,'\AppData\Roaming\MathWorks\MATLAB\R',release,''];
prevDir = cd(historyLocation);

%% Initialise the Git repository (a hidden subfolder called .git)
commandLine1 = 'git init';
[statusInit, resultInit] = system(commandLine1);

%% Add all the files in the user's MATLAB settings to the Git repository
commandLine2 = 'git add .';
[statusAdd, resultAdd] = system(commandLine2);

%% Go back where we came from
cd(prevDir);
