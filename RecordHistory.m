function [result] = RecordHistory()

%RecordHistory Adds the MATLAB Command History to the current Git repository

% Use in conjunction with finish.m and AddGitRepo.m
% Alex Henderson (alex.henderson @ manchester.ac.uk) 2014
% Released under Creative Commons CC-BY licence
% See https://bitbucket.org/AlexHenderson/matlabhistory

% Requires an installation of Git available on the system path (not the
% MATLAB path)
% More information on Git version control here: http://git-scm.com/

% Version 1.0 2014-04-15
%   initial release

%% Check we're running Windows. These commands are OS specific. 
if (~ispc)
    error('These steps are for Windows only. Similar mechanisms may be available for Linux and Macintosh');
end

%% Gather pertinent information about the user and MATLAB version
homeLocation=getenv('USERPROFILE');
release=version('-release');

%% Move to location of the MATLAB Command History
historyLocation = [homeLocation,'\AppData\Roaming\MathWorks\MATLAB\R',release,''];
prevDir = cd(historyLocation);

%% Generate commit message based on the date
message = ['Updated: ', datestr(now)];

%% Commit current history to the Git repository
commandLine = ['git commit -am "', message, '"'];
[status, result] = system(commandLine);

%% Go back where we came from
cd(prevDir);
